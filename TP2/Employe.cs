﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2
{
    class Employe : Personne
    {
        private int salaire;

        public int Salaire
        {
            get { return salaire; }
            set { salaire = value; }
        }

        public Employe(string nom, string prenom, int dateNaissance, int salaire) : base(nom, prenom, dateNaissance)
        {
            Salaire = salaire;
        }

        public override string Afficher()
        {
            return String.Format("{0} - {1}", base.Afficher(), Salaire);
        }
    }
}
