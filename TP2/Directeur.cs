﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2
{
    class Directeur : Chef
    {
        private string societe;

        public string Societe
        {
            get { return societe; }
            set { societe = value; }
        }

        public Directeur(string nom, string prenom, int dateNaissance, int salaire, string service, string societe) : base(nom, prenom, dateNaissance, salaire, service)
        {
            Societe = societe;
        }

        public override string Afficher()
        {
            return String.Format("{0} - {1}", base.Afficher(), Societe);
        }
    }
}
