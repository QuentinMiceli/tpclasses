﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2
{
    class Chef : Employe
    {
        private string service;

        public string Service
        {
            get { return service; }
            set { service = value; }
        }

        public Chef(string nom, string prenom, int dateNaissance, int salaire, string service) : base(nom, prenom, dateNaissance, salaire)
        {
            Service = service;
        }

        public override string Afficher()
        {
            return String.Format("{0} - {1}", base.Afficher(), Service);
        }
    }
}
