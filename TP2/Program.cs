﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2
{
    class Program
    {
        static void Main(string[] args)
        {
            Employe e1 = new Employe("Dupont", "Jean", 1958, 1632);
            Employe e2 = new Employe("Dupont", "Marie", 1989, 1324);
            Employe e3 = new Employe("Dupont", "Mario", 1945, 1354);
            Employe e4 = new Employe("Dupont", "Antho", 1932, 1260);
            Employe e5 = new Employe("Dupont", "Lucie", 1956, 1632);

            Chef c1 = new Chef("Dumas", "Michel", 1945, 3000, "Monetique");
            Chef c2 = new Chef("Dubois", "Pierre", 1945, 3564, "Logistique");

            Directeur d1 = new Directeur("Goldberg", "David", 1935, 123432, "Direction", "Total");

            Personne[] equipe = new Personne[8] {e1, e2, e3, e4, e5, c1, c2, d1 };

            for (int i = 0; i < equipe.Length; i++)
            {
                Console.WriteLine(equipe[i].Afficher());
            }
            Console.WriteLine();
            foreach (Personne personne in equipe)
            {
                Console.WriteLine(personne.Afficher());
            }
        }
    }
}
