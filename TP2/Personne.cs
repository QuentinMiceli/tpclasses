﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2
{
    abstract class Personne
    {
        private string nom;
        private string prenom;
        private int dateNaissance;

        public string Nom
        {
            get { return nom; }
            set { nom = value; }
        }
        public string Prenom
        {
            get { return prenom; }
            set { prenom = value; }
        }
        public int DateNaissance
        {
            get { return dateNaissance; }
            set { dateNaissance = value; }
        }

        public Personne(string nom, string prenom, int dateNaissance)
        {
            Nom = nom;
            Prenom = prenom;
            DateNaissance = dateNaissance;
        }

        public virtual string Afficher()
        {
            return String.Format("{0} {1} : né en {2}", Nom, Prenom, DateNaissance);
        }

    }
}
