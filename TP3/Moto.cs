﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP3
{
    class Moto
    {
        public int Code { get; set; }
        public int PuissanceMoteur { get; set; }
        public string Couleur { get; set; }
        public int Vitesse { get; set; }
        public string Constructeur { get; set; }

        public Moto(int code, int puissanceMoteur, string couleur, int vitesse, string constructeur)
        {
            Code = code;
            PuissanceMoteur = puissanceMoteur;
            Couleur = couleur;
            Vitesse = vitesse;
            Constructeur = constructeur;
        }

        public static Moto Saisie()
        {
            Console.WriteLine("Entrez le code de la voiture : ");
            int code = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Entrez la puissance du moteur : ");
            int puissanceMoteur = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Entrez la couleur : ");
            string couleur = Console.ReadLine();
            Console.WriteLine("Entrez la vitesse : ");
            int vitesse = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Entrez le constructeur : ");
            string constructeur = Console.ReadLine();

            return new Moto(code, puissanceMoteur, couleur, vitesse, constructeur);
        }
        public override string ToString()
        {
            return String.Format("{0} {1} {2} {3} {4}", Code, PuissanceMoteur, Couleur, Vitesse, Constructeur);
        }
    }
}
