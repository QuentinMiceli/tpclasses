﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP3
{
    class Voiture
    {
        public int Code { get; set; }
        public int PuissanceMoteur { get; set; }
        public string Couleur { get; set; }
        public int NbPlaces { get; set; }
        public string Constructeur { get; set; }

        public Voiture(int code, int puissanceMoteur, string couleur, int nbPlaces, string constructeur)
        {
            Code = code;
            PuissanceMoteur = puissanceMoteur;
            Couleur = couleur;
            NbPlaces = nbPlaces;
            Constructeur = constructeur;
        }

        public static Voiture Saisie()
        {
            Console.WriteLine("Entrez le code de la voiture : ");
            int code = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Entrez la puissance du moteur : ");
            int puissanceMoteur = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Entrez la couleur : ");
            string couleur = Console.ReadLine();
            Console.WriteLine("Entrez le Nb de places : ");
            int nbPlaces = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Entrez le constructeur : ");
            string constructeur = Console.ReadLine();

            return new Voiture(code, puissanceMoteur, couleur, nbPlaces, constructeur);
        }
        public override string ToString()
        {
            return String.Format("{0} {1} {2} {3} {4}", Code, PuissanceMoteur, Couleur, NbPlaces, Constructeur);
        }
    }
}
