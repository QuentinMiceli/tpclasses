﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP3
{
    interface IVehiculeListe
    {
        int Moyenne();
        void Affichage();
        void Ajout();
        object Recherche();
        int Max();
    }
}
