﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP3
{
    class Camion
    {
        public int Code { get; set; }
        public int PuissanceMoteur { get; set; }
        public string Couleur { get; set; }
        public int Capacite { get; set; }
        public string Constructeur { get; set; }

        public Camion(int code, int puissanceMoteur, string couleur, int capacite, string constructeur)
        {
            Code = code;
            PuissanceMoteur = puissanceMoteur;
            Couleur = couleur;
            Capacite = capacite;
            Constructeur = constructeur;
        }

        public static Camion Saisie()
        {
            Console.WriteLine("Entrez le code de la voiture : ");
            int code = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Entrez la puissance du moteur : ");
            int puissanceMoteur = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Entrez la couleur : ");
            string couleur = Console.ReadLine();
            Console.WriteLine("Entrez la capacite : ");
            int capacite = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Entrez le constructeur : ");
            string constructeur = Console.ReadLine();

            return new Camion(code, puissanceMoteur, couleur, capacite, constructeur);
        }
        public override string ToString()
        {
            return String.Format("{0} {1} {2} {3} {4}", Code, PuissanceMoteur, Couleur, Capacite, Constructeur);
        }
    }
}
