﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TP1
{
    class Compte
    {
        private int code;
        private int solde;
        private Client client;

        public static int refCode;
        public static int counter = 0;

        public int Code
        {
            get { return code; }
        }
        public int Solde
        {
            get { return solde; }
        }
        public Client Client
        {
            get { return client; }
            set { client = value; }
        }

        public Compte(Client client)
        {
            Client = client;
            code = Interlocked.Increment(ref refCode);
            solde = 0;
            Interlocked.Increment(ref counter);
        }

        public void Crediter(int somme)
        {
            solde += somme;
        }
        public void Crediter(int somme, Compte compte)
        {
            compte.solde -= somme;
            solde += somme;
        }
        public void Debiter(int somme)
        {
            solde -= somme;
        }
        public void Debiter(int somme, Compte compte)
        {
            compte.solde += somme;
            solde -= somme;
        }
        public void Afficher()
        {
            Console.WriteLine(String.Format("{0} - {1} {2} | Nb: {3} - {4}", Client.Cin, Client.Nom, Client.Prenom, Code, Solde));
        }
        public static void Compter()
        {
            Console.WriteLine(String.Format("{0} comptes existent", counter));
        }

    }
}
