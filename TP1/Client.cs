﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP1
{
    class Client
    {
        private string cin;
        private string nom;
        private string prenom;
        private string tel;

        public string Cin
        {
            get { return cin; }
            set { cin = value; }
        }
        public string Nom
        {
            get { return nom; }
            set { nom = value; }
        }
        public string Prenom
        {
            get { return prenom; }
            set { prenom = value; }
        }
        public string Tel
        {
            get { return tel; }
            set { tel = value; }
        }

        public Client(string cin, string nom, string prenom, string tel)
        {
            Cin = cin;
            Nom = nom;
            Prenom = prenom;
            Tel = tel;
        }
        public Client(string cin, string nom, string prenom)
        {
            Cin = cin;
            Nom = nom;
            Prenom = prenom;
        }

        public void Afficher()
        {
            Console.WriteLine(String.Format("{0} - {1} {2} {3}", Cin, Nom, Prenom, tel));
        }
    }
}
