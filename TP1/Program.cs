﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP1
{
    class Program
    {
        static void Main(string[] args)
        {
            Client c1 = new Client("EDFE6546", "Miceli", "Quentin", "1654231");
            Compte cpt1 = new Compte(c1);
            Compte cpt2 = new Compte(c1);

            cpt1.Crediter(200);
            cpt2.Crediter(20, cpt1);
            cpt2.Debiter(5, cpt1);

            c1.Afficher();
            cpt1.Afficher();
            cpt2.Afficher();

            Compte.Compter();
        }
    }
}
